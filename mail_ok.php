<!DOCTYPE html>
<html lang="pt">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />


	<title>Eletroguara Materiais Elétricos</title>

	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/estilo.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" crossorigin="anonymous"/>

	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

</head>
<body>
	<div id="header"></div>

    <div class="container-fluid corpo-email d-flex justify-content-center align-items-center flex-column">
      <p class=" alert alert-success">Mensagem enviada com sucesso.</p>
      <a href="https://eletroguara.ideiapc.com.br/index.html" class="btn btn-primary">VOLTAR </a>
    </div>
	<!-- FOOTER.HTML -->
	<div id="footer"></div>	
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"> </script>
	<script src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/lozad/dist/lozad.min.js"></script>


	<script>
  lozad().observe();

		$(function(){ 
			$("#header").load("header.html");
			$("#footer").load("footer.html"); 
		});
	</script>
</body>
</html>