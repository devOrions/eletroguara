<?php
// Destinatário
$para = "vendas@eletroguara.com.br";
// Assunto do e-mail
$assunto = "[SITE]";

// Campos do formulário de contato
$nome = $_POST['nome'];
$email = $_POST['email'];
$telefone = $_POST['telefone'];
$mensagem = $_POST['mensagem'];

// Monta o corpo da mensagem com os campos
$corpo = "<html><body>";
$corpo .= "<strong>Nome</strong>: $nome ";
$corpo .= "<strong> <br/> Email</strong>: $email <strong><br/>Telefone</strong>: $telefone <strong><br/>Mensagem</strong>: <br/>$mensagem";
$corpo .= "</body></html>";

$corpo = "<html>
<style>
.wrapper{  
  background: #fefbf1;
}
.imagem{
  padding-top: 15px;
  text-align: center;
}
.wrapper > p{
  text-align:center;
  padding-top: 0px;
}
.wrapper p a{
  text-decoration: none;
}
.wrapper > h4{
  background: #733C19;
  color: #fff;
  padding: 5px 0 5px 10px;

}

.inside{
  display:flex;
  justify-content: flex-start;
  align-items:center;
  flex-wrap: wrap;
  padding: 0px 10px 10px 10px;

}
.inside h5{
  text-align: left;
}
.inside p{
  font-size: 0.9em;
}
.inside a {
  text-decoration: none;
}

.mensagem{
  display:flex !important;
  flex-direction: column !important;
  justify-content: flex-start !important;
  align-items:flex-start !important;
  flex-wrap: wrap !important;
}
</style>
<body>";
$corpo .= 
" <div class='wrapper'>
<div class='imagem'>
  <img src='https://eletroguara.ideiapc.com.br/img/logo-footer.png' alt='Logotipo Eletroguara'>
</div>

<p>Novo orçamento do site <a href='https://eletroguara.ideiapc.com.br/index.html' target='_blank'>ELETROGUARA</a></p>
<h4>DADOS DO CLIENTE</h4>
<div class='inside'>
 <h4 ><strong>Nome</strong>:</h4>
 <p>&nbsp $nome</p>  
</div>
<div class='inside'>
 <h4 ><strong>Email</strong>: </h4>
 <a href='mailto:$email' target='_blank'>&nbsp $email</a>  
</div>
<div class='inside'>
 <h4 ><strong>Telefone</strong>: </h4>
 <p>&nbsp $telefone</p>  
</div>
<div class='inside mensagem'>
 <h4 ><strong>Mensagem</strong>:</h4>
 <p> &nbsp  $mensagem </p>  
</div>
</div>  
";

$corpo .= "</body></html>";


// Cabeçalho do e-mail
$email_headers = implode("\n", array("From: $nome", "Reply-To: $email", "Subject: $assunto", "Return-Path: $email", "MIME-Version: 1.0", "X-Priority: 3", "Content-Type: text/html; charset=UTF-8"));


//Verifica se os campos estão preenchidos para enviar então o email
if (!empty($nome) && !empty($email) && !empty($telefone) && !empty($mensagem)) {
  mail($para, $assunto, $corpo, $email_headers);
  $pagina = "mail_ok.php";
} else {
  $pagina = "mail_error.php";
}

header("location:$pagina");

?>